# HW 1: Make a mini-project

## Project Name: 
asciilines
---
## Copyright Notice: 

Copyright © <2019> Daniel Connelly. All rights reserved.
---
## Explanation of what the program is and does

The program takes an <em>n</em> number lines of input and,
for each line of input, interprets each element (besides
spaces) as a command to print out a matrix-type output to
the screen.

Initially all matrices are initialized to "." character.

Each file consists of two lines. The first line indicates the
"canvas size" (i.e., row and column) of the program and each 
must be > 0 (or rather the next lines arguments must be greater
than or equal to it).

The next line consits of ofur characters that we treat as 
commands to our program. 

The first character in the input stream/file is the symbol
to replace these characters with. The second character is
the row position to start at (0-based) and the third character
is the column to start at (0-based). The next character is 
either an 'h' or a 'v', and indicates whether the program will
fill the matrice with the symbol in a "downward" or "horizontal"
direction. Finally, the fourth character symbolizes the length
(greater than 0) that we render the line in our program for.

---
## Explanation of how to build and run the program

Use the following format for running the program:

*python3 asciilines.py [path to tvg file]

Alternatively, if you would like to run a test (or create
and run a test of your own), create a .tvg file in the test
directory and run the program as such:

*python3 asciilines.py tests/test[x].tvg*.
---
## Information about bugs, defects, failing tests, etc

There are no known bugs, defects, or failing tests known at
this time.

---
## License information
(COPY OF LICENSE file):

[This program is licensed under the "MIT License"]

> Permission is hereby granted, free of charge, to any person
> obtaining a copy of this software and associated
> documentation files (the "Software"), to deal in the
> Software without restriction, including without limitation
> the rights to use, copy, modify, merge, publish, distribute,
> sublicense, and/or sell copies of the Software, and to
> permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> The above copyright notice and this permission notice shall
> be included in all copies or substantial portions of the
> Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
> KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
> WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
> PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
> OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
> OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
> OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
> SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
