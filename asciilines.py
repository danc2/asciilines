import sys
import numpy as np

# Num of arguments, .tvg file
def errorChecks():
    # test if we have correct # of arguments
    length_of_file = len(sys.argv)
    if length_of_file < 0:
        print("No command-line arguments found. Please enter 1 command-line argument"); sys.exit()

    # test if file is a .tvg file
    lastThreeChars = sys.argv[1][length_of_file-6:]
    if lastThreeChars != ".tvg":
        print("Last three chars did not replicate the \".tvg\" pattern (entered '{}'). Please enter a .tvg file".format(sys.argv[1])); sys.exit()

# Canvas size
def errorChecks2(line):
    try:
        if int(line[0]) < 0 or int(line[1]) < 0:
            print("Canvas size must be > 0, but was {0} and {1}".format(line[0], line[2])); sys.exit()
            sys.exit();
    except:
        print("An exception occured when processing the canvas size to test if it was within bounds. Likely the canvas were symbols or letters, not ints in String or Int format.")
        sys.exit()

# render the given file
def render(thisline, matrix):
    symbol = thisline[0]; row = int(thisline[1]); col = int(thisline[2]); direction = thisline[3]; distance = int(thisline[4])
    shape = matrix.shape
    maxRow = shape[0]; maxCol = shape[1]
    # one last error check... could be in a function as the error checks before
    if row > maxRow or col > maxCol:
        print("ERROR IN RENDERER: row or column start point exceeds the size of the matrix.")
        sys.exit(2)
    # we change columns
    if direction == "h":
        matrix[row, col] = symbol
        for i in range(1, distance):
            if col+1 != maxCol: col = col+1 # we must be careful to not reach out of the bounds of our array. If we do, we set col = 0 (see below)
            else: col = 0; matrix[row, col] = symbol; continue
            matrix[row, col] = symbol
        return
    # we change rows
    elif direction == "v":
        matrix[row, col] = symbol
        for i in range(1, distance):
            if row+1 != maxRow: row = row+1
            else: row = 0; matrix[row, col] = symbol; continue
            matrix[row, col] = symbol
        return
    else:
        print("Did not enter a correct direction. Should be 'h' or 'v'.");
        sys.exit();

# print ASCII
def printASCII(matrix):
    shape = matrix.shape
    for i in range(shape[0]):
        for j in range(shape[1]):
            print(str(matrix[i,j]), end="")
        print()

def main():
    # pre-file error checks
    errorChecks()
    canvasCheck = True
    # read in file
    try:
        with open(sys.argv[1], "r") as f:
            for line in f:
                thisline = line.split(" ")
                # file canvas-size error checks and matrix creation
                if canvasCheck == True:
                    errorChecks2(thisline)
                    matrix = np.full([int(thisline[0]),int(thisline[1])],".") # array to keep our data and print it out to standard output (later)
                    canvasCheck = False
                    continue
                # render the given line
                render(thisline, matrix)
        # print the .tvg output
        printASCII(matrix)
    except Exception as error:
        print(error)

if __name__ == "__main__":
    main()
